'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Customer extends Model {
    static associate(models) {
      this.hasMany(models.Order, {
        foreignKey: 'customerId'
      })
    }
  };
  Customer.init({
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: DataTypes.STRING,
    address: DataTypes.STRING
  }, {
    sequelize,
    tableName: 'customers',
    modelName: 'Customer'
  });
  return Customer;
};