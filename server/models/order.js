'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    static associate(models) {
      this.belongsTo(models.Customer, { 
        foreignKey: 'customerId',
        targetKey: 'id'
      });
      this.belongsTo(models.Product, { 
        foreignKey: 'productId',
        targetKey: 'id'
      });
    }
  };
  Order.init({
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    productId: {
      type: DataTypes.INTEGER,
      foreignKey: true
    },
    customerId: {
      type: DataTypes.INTEGER,
      foreignKey: true
    },
    qty: DataTypes.INTEGER,
    createdAt: new Date()
  }, {
    sequelize,
    tableName: 'orders',
    modelName: 'Order',
  });
  return Order;
};