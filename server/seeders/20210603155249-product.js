'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('products', [
      {
        title: 'Ba Xo',
        description: 'We Nak Xen To Sha',
        price: 49000,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Xio May',
        description: 'Zha Min Mak Nyu S',
        price: 34000,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Xe Tik',
        description: 'Xa Pi A Fong',
        price: 79000,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('product', null, {});
  }
};
