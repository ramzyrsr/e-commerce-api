'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('orders', [
      {
        productId: 1,
        customerId: 2,
        qty: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        productId: 2,
        customerId: 3,
        qty: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        productId: 3,
        customerId: 1,
        qty: 4,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('order', null, {});
  }
};
