'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('customers', [
      {
        name: 'Xi Lao',
        address: 'Zhong Wan',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Wang Xit',
        address: 'Xe Do Ya',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Xa Pi',
        address: 'Pik Xa Tu',
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Customer', null, {});
  }
};
