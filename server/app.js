const express = require('express');
const bp = require('body-parser');
const logger = require('morgan');
const cors = require('cors');

const app = express();

if (process.env.NODE_ENV === 'development') {
    app.use(logger('dev'));
};

app.use(cors());
app.use(bp.urlencoded({ extended: false}));
app.use(bp.json());

const orders = require('./routes/ordersRoute');
const products = require('./routes/productsRoute');
const customers = require('./routes/customersRoute');

app.use('/api/v1', orders);
app.use('/api/v1', products);
app.use('/api/v1', customers);

module.exports = app;