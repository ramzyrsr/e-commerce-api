const Models = require('../models');
const methods = {};

methods.getOrder = async (req, res) => {
    try {
        const order = await Models.Customer.findOne({
            where: {
                id: req.params.id
            },
            include: [{ model: Models.Order,
                include: [Models.Product] }],
        })

        const resPayload = {
            statusCode: 200,
            statusText: 'Success',
            message: 'Get Order',
            data: order
        };
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        res
            .json(error)
            .status(404);
    }
};

methods.getAllOrders = async (req, res) => {
    try {
        const orders = await Models.Customer.findAll({
            include: [{ model: Models.Order,
                include: [Models.Product] }],
        });
        const resPayload = {
            statusCode: 200,
            statusText: 'Success',
            message: 'Get All Orders',
            data: orders
        }
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        console.log(error)
        res
            .json(error)
            .status(404);
    }
};

methods.createOrder = async (req, res) => {
    try {
        const reqPayload = {
            title: req.body.title,
            description: req.body.description,
            price: req.body.price,
            timeStamp
        };

        const newOrder = Order.create(reqPayload);
        if (newOrder) {
            res
                .json({
                    statusCode: 201,
                    statusText: 'Created',
                    message: 'Order is created',
                    data: reqPayload
                })
                .status(201);
        }

    } catch (error) {
        res
            .json({
                statusCode: 400,
                statusText: 'Bad Request',
                message: error
            })
            .status(400);
    }
};

methods.updateOrder = async (req, res) => {
    try {
        await Order.update(req.body, {
            where: {
                id: req.params.id
            }
        });

        res
            .json({
                statusCode: 200,
                statusText: 'Success',
                message: 'Order is updated'
            })
            .status(200);

    } catch (error) {
        res
            .json({
                statusCode: 400,
                statusText: 'Bad Request',
                message: error
            })
            .status(400);
    }
};

methods.deleteOrder = async (req, res) => {
    try {
        await Order.destroy({
            where: {
                id: req.params.id
            }
        })

        res
            .json({
                statusCode: 204,
                statusText: 'No Content',
                message: 'Order is deleted'
            })
            .status(204);

    } catch (error) {
        res
            .json({
                statusCode: 400,
                statusText: 'Bad Request',
                message: error
            })
            .status(400);
    }
};

module.exports = methods;