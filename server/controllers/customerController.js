const Models = require('../models');
const { timeStamp } = require('console');
const methods = {};

methods.getCustomer = async (req, res) => {
    try {
        const customer = await Models.Customer.findOne({
            where: {
                id: req.params.id
            }
        })

        const resPayload = {
            statusCode: 200,
            statusText: 'Success',
            message: 'Get Customer',
            data: customer
        };
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        res
            .json(error)
            .status(404);
    }
};

methods.getAllCustomers = async (req, res) => {
    try {
        const customers = await Models.Customer.findAll();
        const resPayload = {
            statusCode: 200,
            statusText: 'Success',
            message: 'Get All Customers',
            data: customers
        }
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        res
            .json(error)
            .status(404);
    }
};

methods.createCustomer = async (req, res) => {
    try {
        const reqPayload = {
            title: req.body.title,
            description: req.body.description,
            price: req.body.price,
            timeStamp
        };

        const newCustomer = Models.Customer.create(reqPayload);
        if (newCustomer) {
            res
                .json({
                    statusCode: 201,
                    statusText: 'Created',
                    message: 'Customer is created',
                    data: reqPayload
                })
                .status(201);
        }

    } catch (error) {
        res
            .json({
                statusCode: 400,
                statusText: 'Bad Request',
                message: error
            })
            .status(400);
    }
};

methods.updateCustomer = async (req, res) => {
    try {
        await Models.Customer.update(req.body, {
            where: {
                id: req.params.id
            }
        });

        res
            .json({
                statusCode: 200,
                statusText: 'Success',
                message: 'Customer is updated'
            })
            .status(200);

    } catch (error) {
        res
            .json({
                statusCode: 400,
                statusText: 'Bad Request',
                message: error
            })
            .status(400);
    }
};

methods.deleteCustomer = async (req, res) => {
    try {
        await Models.Customer.destroy({
            where: {
                id: req.params.id
            }
        })

        res
            .json({
                statusCode: 204,
                statusText: 'No Content',
                message: 'Customer is deleted'
            })
            .status(204);

    } catch (error) {
        res
            .json({
                statusCode: 400,
                statusText: 'Bad Request',
                message: error
            })
            .status(400);
    }
};

module.exports = methods;