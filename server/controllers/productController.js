const Product = require('../models/product');
const { timeStamp } = require('console');
const methods = {};

methods.getProduct = async (req, res) => {
    try {
        const product = await Product.findOne({
            where: {
                id: req.params.id
            }
        })

        const resPayload = {
            statusCode: 200,
            statusText: 'Success',
            message: 'Get Product',
            data: product
        };
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        res
            .json(error)
            .status(404);
    }
};

methods.getAllProducts = async (req, res) => {
    try {
        const products = await Product.findAll();
        const resPayload = {
            statusCode: 200,
            statusText: 'Success',
            message: 'Get All Products',
            data: products
        }
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        res
            .json(error)
            .status(404);
    }
};

methods.createProduct = async (req, res) => {
    try {
        const reqPayload = {
            title: req.body.title,
            description: req.body.description,
            price: req.body.price,
            timeStamp
        };

        const newProduct = Product.create(reqPayload);
        if (newProduct) {
            res
                .json({
                    statusCode: 201,
                    statusText: 'Created',
                    message: 'Product is created',
                    data: reqPayload
                })
                .status(201);
        }

    } catch (error) {
        res
            .json({
                statusCode: 400,
                statusText: 'Bad Request',
                message: error
            })
            .status(400);
    }
};

methods.updateProduct = async (req, res) => {
    try {
        await Product.update(req.body, {
            where: {
                id: req.params.id
            }
        });

        res
            .json({
                statusCode: 200,
                statusText: 'Success',
                message: 'Product is updated'
            })
            .status(200);

    } catch (error) {
        res
            .json({
                statusCode: 400,
                statusText: 'Bad Request',
                message: error
            })
            .status(400);
    }
};

methods.deleteProduct = async (req, res) => {
    try {
        await Product.destroy({
            where: {
                id: req.params.id
            }
        })

        res
            .json({
                statusCode: 204,
                statusText: 'No Content',
                message: 'Product is deleted'
            })
            .status(204);

    } catch (error) {
        res
            .json({
                statusCode: 400,
                statusText: 'Bad Request',
                message: error
            })
            .status(400);
    }
};

module.exports = methods;