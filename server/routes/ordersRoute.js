const express = require('express');
const router = express.Router();
const controller = require('../controllers/orderController');

router
    .route('/order')
    .get(controller.getAllOrders)
    .post(controller.createOrder);

router
    .route('/order/:id')
    .get(controller.getOrder)
    .put(controller.updateOrder)
    .delete(controller.deleteOrder);

module.exports = router;