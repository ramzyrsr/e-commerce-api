const express = require('express');
const router = express.Router();
const controller = require('../controllers/productController');

router
    .route('/product')
    .get(controller.getAllProducts)
    .post(controller.createProduct);

router
    .route('/product/:id')
    .get(controller.getProduct)
    .put(controller.updateProduct)
    .delete(controller.deleteProduct);

module.exports = router;