const express = require('express');
const router = express.Router();
const controller = require('../controllers/customerController');

router
    .route('/customer')
    .get(controller.getAllCustomers)
    .post(controller.createCustomer);

router
    .route('/customer/:id')
    .get(controller.getCustomer)
    .put(controller.updateCustomer)
    .delete(controller.deleteCustomer);

module.exports = router;