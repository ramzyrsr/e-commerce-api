// require('dotenv').config();
const app = require('./app');

console.log(app.get('env'));

const port = 8000;
app.listen(port, (err) => {
    if (err) {
        console.log(err);
    };
    console.log('Running on port: ', port);
})